# C driver for PinePhone Fingerprint reader

This is a C implementation of the [PinePhone fingerprint reader](https://wiki.pine64.org/wiki/PinePhone_%28Pro%29_Add-ons#Fingerprint_Reader_Add-on).
The sensor is a [PixelAuth PIA Module](https://files.pine64.org/doc/datasheet/pinephone/Datasheet_PixelAuth_PIA_Module_P2SDS-NABL2-S05_V7.0.0.5.pdf) using the protocol described [here](https://github.com/zschroeder6212/tiny-i2c-spi/files/7996765/command.spec.EN.pdf) and [here](https://github.com/zschroeder6212/tiny-i2c-spi/issues/2).
It communicates with the phone using [zschroeder6212](https://github.com/zschroeder6212/) [I2C-SPI bridge](https://github.com/zschroeder6212/tiny-i2c-spi/).

This implementation is partly base on [this python code](https://github.com/egormanga/pinephone-fingerprint/).

## Building
pia requires openssl for some random key generation and HMAC checking.
Also make sure that you have installed the kernel headers for `line/i2c-dev.h` to be available.
On postmarketOS you can run
```shell
$ sudo apk add openssl-dev linux-headers
$ make
$ sudo make install
```

## Usage
To use pia, first generate and store an HMAC key.
This is usually stored in `/etc/pia/hmac`
```shell
$ sudo mkdir /etc/pia/
$ sudo pia -g                 # store hmac key in /etc/pia/hmac
# or
$ pia -k /path/to/hmac/key -g # store hmac key elsewhere
```

### Enrolling
You need to enrol your fingerprints into the chip. To do that, run
```shell
$ pia -e <uid>
Status: Wait for finger press
Status: Only partial image captured
Status: Wait for finger press
Status: Good image capture, next image
Status: Wait for finger press
Status: Good image capture, next image
Status: Wait for finger press
Status: Good image capture, next image
Status: Wait for finger press
Status: Good image capture, next image
Status: Good image capture, next image
Status: Wait for finger press
Status: Good image capture, next image
Status: Wait for finger press
Status: Good image capture, next image
Status: Wait for finger press
Status: Good image capture, next image
Status: Wait for finger press
Status: Good image capture, next image
Status: Wait for finger press
Status: Good image capture, next image
Status: Wait for finger press
Status: Good image capture, next image
Status: Wait for finger press
Status: Good image capture, next image
Status: Wait for finger press
Status: Good image capture, next image
Status: Wait for finger press
Status: Good image capture, next image
Status: Wait for finger press
Status: Good image capture, next image
Status: Success
```
`uid` is a string identifying the user with up to 32 bytes (automatically padded with zeros).
You can enrol multiple fingers to the same `uid`.

### Managing users
Use `pia -l` to list enrolled `uid`s and `pia -d` to delete them
```shell
$ pia -l
Found 4 fingers:
Finger 0
Finger 1
Finger 2
Finger 3
$ pia -d 3
$ pia -l
Found 3 fingers:
Finger 0
Finger 1
Finger 2
```

### Verifying
`pia -c` returns 0 if the verification is successful and 1 otherwise
```shell
$ if pia -c 0; then
>   echo "Success"
> else
>   echo "Fail"
> fi
Fail
```

## phosh
Using this with phosh is a bit tricky because of how it manages login.
Phosh doesn't currently (as of [0.16.0](https://gitlab.gnome.org/World/Phosh/phosh/-/tree/v0.16.0) support programmatic unlocking (see also [phosh#558](https://gitlab.gnome.org/World/Phosh/phosh/-/issues/558)).
The password is checked with PAM but it still needs to be provided.
This means that [fprint](https://fprint.freedesktop.org/) won't work either.
The only solution is to patch phosh and recompile it.
$2279542 provides a suitable patch and an APKBUILD file for postmarketOS.

### Setup in phosh
Setup pia as described above and then set your full, 32 byte uid
```shell
$ gsettings set sm.puri.phosh.lockscreen pia-uid '00000000000000000000000000000000'
```
To disable pia again, just set `pia-uid` to an empty string
```shell
$ gsettings set sm.puri.phosh.lockscreen pia-uid ''
```
