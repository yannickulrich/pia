#include "pia.h"
#include <stdio.h>
#include <linux/i2c-dev.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/file.h>
#include <time.h>

#define MAX_LENGTH 0x110
#define READ_LENGTH (8 + MAX_LENGTH + 2)

__attribute__ ((visibility("default")))
enum verbosity pia_verbosity = ERR;

int fd_lock;
const char HEADER[3] = "\x55\xaa\x00";

uint16_t crc16(uint8_t *mem, uint8_t len)
{
    const uint16_t reflect_table[256] = {
        0x00, 0x80, 0x40, 0xC0, 0x20, 0xA0, 0x60, 0xE0,
        0x10, 0x90, 0x50, 0xD0, 0x30, 0xB0, 0x70, 0xF0,
        0x08, 0x88, 0x48, 0xC8, 0x28, 0xA8, 0x68, 0xE8,
        0x18, 0x98, 0x58, 0xD8, 0x38, 0xB8, 0x78, 0xF8,
        0x04, 0x84, 0x44, 0xC4, 0x24, 0xA4, 0x64, 0xE4,
        0x14, 0x94, 0x54, 0xD4, 0x34, 0xB4, 0x74, 0xF4,
        0x0C, 0x8C, 0x4C, 0xCC, 0x2C, 0xAC, 0x6C, 0xEC,
        0x1C, 0x9C, 0x5C, 0xDC, 0x3C, 0xBC, 0x7C, 0xFC,
        0x02, 0x82, 0x42, 0xC2, 0x22, 0xA2, 0x62, 0xE2,
        0x12, 0x92, 0x52, 0xD2, 0x32, 0xB2, 0x72, 0xF2,
        0x0A, 0x8A, 0x4A, 0xCA, 0x2A, 0xAA, 0x6A, 0xEA,
        0x1A, 0x9A, 0x5A, 0xDA, 0x3A, 0xBA, 0x7A, 0xFA,
        0x06, 0x86, 0x46, 0xC6, 0x26, 0xA6, 0x66, 0xE6,
        0x16, 0x96, 0x56, 0xD6, 0x36, 0xB6, 0x76, 0xF6,
        0x0E, 0x8E, 0x4E, 0xCE, 0x2E, 0xAE, 0x6E, 0xEE,
        0x1E, 0x9E, 0x5E, 0xDE, 0x3E, 0xBE, 0x7E, 0xFE,
        0x01, 0x81, 0x41, 0xC1, 0x21, 0xA1, 0x61, 0xE1,
        0x11, 0x91, 0x51, 0xD1, 0x31, 0xB1, 0x71, 0xF1,
        0x09, 0x89, 0x49, 0xC9, 0x29, 0xA9, 0x69, 0xE9,
        0x19, 0x99, 0x59, 0xD9, 0x39, 0xB9, 0x79, 0xF9,
        0x05, 0x85, 0x45, 0xC5, 0x25, 0xA5, 0x65, 0xE5,
        0x15, 0x95, 0x55, 0xD5, 0x35, 0xB5, 0x75, 0xF5,
        0x0D, 0x8D, 0x4D, 0xCD, 0x2D, 0xAD, 0x6D, 0xED,
        0x1D, 0x9D, 0x5D, 0xDD, 0x3D, 0xBD, 0x7D, 0xFD,
        0x03, 0x83, 0x43, 0xC3, 0x23, 0xA3, 0x63, 0xE3,
        0x13, 0x93, 0x53, 0xD3, 0x33, 0xB3, 0x73, 0xF3,
        0x0B, 0x8B, 0x4B, 0xCB, 0x2B, 0xAB, 0x6B, 0xEB,
        0x1B, 0x9B, 0x5B, 0xDB, 0x3B, 0xBB, 0x7B, 0xFB,
        0x07, 0x87, 0x47, 0xC7, 0x27, 0xA7, 0x67, 0xE7,
        0x17, 0x97, 0x57, 0xD7, 0x37, 0xB7, 0x77, 0xF7,
        0x0F, 0x8F, 0x4F, 0xCF, 0x2F, 0xAF, 0x6F, 0xEF,
        0x1F, 0x9F, 0x5F, 0xDF, 0x3F, 0xBF, 0x7F, 0xFF,
    };
    const uint16_t poly = 0x1021;

    uint16_t crc = 0;
    uint8_t *p = mem;

    while(len--)
    {
        crc ^= (reflect_table[*p++] << 8);
        for (uint8_t k = 0; k < 8; k++)
        {
            if(crc & 0x8000)
                crc = (crc << 1) ^ poly;
            else
                crc = (crc << 1);
        }
    }

    return reflect_table[crc >> 8] | reflect_table[crc & 0xff] << 8;

}

__attribute__ ((visibility("default")))
int pia_open(int adapter, int addr)
{
    char filename[20];
    int file;


    fd_lock = open(LOCKFILE, O_CREAT | O_EXCL);
    if(fd_lock < 0)
    {
        if(pia_verbosity >= ERR)
            fprintf(stderr, "[ERR] Lock file %s already exists\n", LOCKFILE);
        return -1;
    }
    if(flock(fd_lock, LOCK_EX) != 0)
    {
        if(pia_verbosity >= ERR)
            fprintf(stderr, "[ERR] Failed to acquire lock on %s\n", LOCKFILE);
        return -1;
    }

    snprintf(filename, 19, "/dev/i2c-%d", adapter);
    file = open(filename, O_RDWR);
    if(file < 0)
    {
        if(pia_verbosity >= ERR)
            fprintf(stderr, "[ERR] Could not open file %s\n", filename);

        pia_close(file);
        return -1;
    }

    if (ioctl(file, I2C_SLAVE, addr) < 0)
    {
        if(pia_verbosity >= ERR)
            fprintf(stderr, "[ERR] Could not ioctl file %s\n", filename);

        pia_close(file);
        return -1;
    }

    return file;
}


__attribute__ ((visibility("default")))
void pia_close(int fd)
{
    if(fd >= 0)      close(fd);
    if(fd_lock >= 0) close(fd_lock);
    unlink(LOCKFILE);
}

int i2c_send(int fd, uint8_t cmd, uint8_t *data, uint8_t len)
{
    if(len > MAX_LENGTH) return -1;

    uint8_t *payload, *p;
    uint16_t checksum;
    payload = p = malloc(11 + len);

    *p++ = 1; // Transmit

    *p++ = HEADER[0];
    *p++ = HEADER[1];
    *p++ = HEADER[2];
    *p++ = cmd;
    *p++ = 0; // Reserved 1
    *p++ = 0; // Reserved 2
    *p++ = len >> 8;
    *p++ = len & 0xff;

    memcpy(p, data, len); p += len;
    checksum = crc16(payload + 1, 8 + len);
    *p++ = checksum >> 8;
    *p++ = checksum & 0xff;

    if(pia_verbosity >= DEBUG)
    {
        p = payload + 1;
        fprintf(stderr, "[INF] > ");
        for(uint8_t i = 0; i < 11 + len; i++)
            fprintf(stderr, "%02x ", *p++);
        fprintf(stderr, "\n");
    }

    write(fd, payload, 11 + len);
    free(payload);

    len += 10;
    uint8_t b;
    while(len--)
        read(fd, &b, 1);
    return 0;
}

uint8_t i2c_read1(int fd)
{
    const char b2[2] = "\x01\x00";
    uint8_t b;
    write(fd, b2, 2);
    read(fd, &b, 1);
    return b;
}

struct response i2c_read_and_verify(int fd, uint8_t cmd, int timeout)
{
    uint16_t i;
    uint8_t headerBuf[3];
    int8_t headerInd;
    uint8_t cmdConfirm, protoCheck, cmdRet, rawLen[2];
    struct response ans;
    uint16_t crc;
    uint8_t *fullBuf;
    time_t start, now;

    ans.len = 0;
    ans.data = NULL;

    if(pia_verbosity >= DEBUG)
        fprintf(stderr, "[INF] Sync...\n");

    start = time(NULL);
    // This reads until we see the header
    headerInd = 0;
    while(1)
    {
        now = time(NULL);
        if( (timeout > 0) && (now - start > timeout) )
        {
            if(pia_verbosity >= ERR)
                fprintf(stderr, "[ERR] Timed out after %lds > %d\n", now-start, timeout);
            ans.status = TIMEOUT;
            return ans;
        }
        headerBuf[headerInd] = i2c_read1(fd);
        headerInd = (headerInd+1)%3;
        if( (headerBuf[(headerInd  )%3] == HEADER[0]) &&
            (headerBuf[(headerInd+1)%3] == HEADER[1]) &&
            (headerBuf[(headerInd+2)%3] == HEADER[2]) )
                break;
    }
    if(pia_verbosity >= DEBUG)
        fprintf(stderr, "[INF] Sync obtained, took %lds...\n[INF] < %02x %02x %02x",
                now-start,
                HEADER[0], HEADER[1], HEADER[2]);

    cmdConfirm = i2c_read1(fd);
    protoCheck = i2c_read1(fd);
    cmdRet = i2c_read1(fd);
    rawLen[0] = i2c_read1(fd);
    rawLen[1] = i2c_read1(fd);

    if(pia_verbosity >= DEBUG)
        fprintf(stderr, "%02x %02x %02x %02x %02x",
                cmdConfirm, protoCheck, cmdRet, rawLen[0], rawLen[1]);



    if((cmdConfirm & 0x7f) != cmd) {
        if(pia_verbosity >= ERR)
            fprintf(stderr, "\n[ERR] incorrect command (got 0x%02x, expected 0x%02x)\n", cmdConfirm & 0x7f, cmd);
        ans.status = I2C_INCORRECT_COMMAND;
        return ans;
    }
    if(!(cmdConfirm & 0x80)) {
        if(pia_verbosity >= ERR)
            fprintf(stderr, "\n[ERR] command 0x%02x failed\n", cmd);
        ans.status = I2C_COMMAND_FAILED;
        return ans;
    }
    if(protoCheck == 0xf1) {
        if(pia_verbosity >= ERR)
            fprintf(stderr, "\n[ERR] Secure level error\n");
        ans.status = I2C_SECURE_LEVEL_ERROR;
        return ans;
    }
    if(protoCheck == 0xf2) {
        if(pia_verbosity >= ERR)
            fprintf(stderr, "\n[ERR] Reserved bytes error\n");
        ans.status = I2C_RESERVED_BYTES_ERROR;
        return ans;
    }
    if(protoCheck == 0xf3) {
        if(pia_verbosity >= ERR)
            fprintf(stderr, "\n[ERR] Checksum error (module side)\n");
        ans.status = I2C_CHECKSUM_ERROR;
        return ans;
    }

    ans.result = cmdRet;

    ans.len = rawLen[0] << 8;
    ans.len |= rawLen[1];

    fullBuf = malloc(ans.len+8);
    fullBuf[0] = HEADER[0];
    fullBuf[1] = HEADER[1];
    fullBuf[2] = HEADER[2];
    fullBuf[3] = cmdConfirm;
    fullBuf[4] = protoCheck;
    fullBuf[5] = cmdRet;
    fullBuf[6] = rawLen[0];
    fullBuf[7] = rawLen[1];

    if(ans.len > 0)
    {
        ans.data = malloc(ans.len);
        for(i=0; i < ans.len; i++)
        {
            fullBuf[i+8] = ans.data[i] = i2c_read1(fd);
            if(pia_verbosity >= DEBUG)
                fprintf(stderr, "%02x ", ans.data[i]);
        }
    }

    crc = i2c_read1(fd) << 8;
    crc |= i2c_read1(fd);
    if(pia_verbosity >= DEBUG)
        fprintf(stderr, "%02x %02x\n", crc >> 8, crc && 0xff);

    if(crc16(fullBuf, ans.len+8) != crc)
    {
        if(pia_verbosity >= ERR)
            fprintf(stderr, "[ERR] Checksum error (host side)\n");
        ans.status = I2C_CHECKSUM_FAIL;
        return ans;
    }

    ans.status = OKAY;

    return ans;
}

struct response i2c_call(int fd, uint8_t cmd, uint8_t *data, uint8_t len, int timeout)
{
    i2c_send(fd, cmd, data, len);
    return i2c_read_and_verify(fd, cmd, timeout);
}
