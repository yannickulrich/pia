CC=gcc
CFLAGS=-fvisibility=hidden -fPIC -g $(shell pkg-config --cflags openssl) -Wall -Werror

LD=gcc
LFLAGS= $(shell pkg-config --libs openssl)

ifeq ($(PREFIX),)
   PREFIX := /usr/local
endif

all: pia libpia.so

%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $<

libpia.so: pia.o i2c.o
	$(LD) -shared -o $@ $^ $(LFLAGS)

pia: libpia.so main.o
	$(LD) -o $@ $^ $(LFLAGS) -L. -lpia

pia.pc: pia.pc.in
	echo "prefix=$(PREFIX)" > $@
	cat $< >> $@

install: libpia.so pia.h pia pia.pc
	install -d $(DESTDIR)$(PREFIX)/lib
	install -m 644 libpia.so $(DESTDIR)$(PREFIX)/lib
	install -d $(DESTDIR)$(PREFIX)/lib/pkgconfig
	install -m 644 pia.pc $(DESTDIR)$(PREFIX)/lib/pkgconfig
	install -d $(DESTDIR)$(PREFIX)/include/
	install -m 644 pia.h $(DESTDIR)$(PREFIX)/include
	install -d $(DESTDIR)$(PREFIX)/bin
	install -m 755 pia $(DESTDIR)$(PREFIX)/bin


clean:
	rm -f pia *.o
