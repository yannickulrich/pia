#include "pia.h"
#include <string.h>
#include <openssl/hmac.h>
#include <openssl/rand.h>
#include <unistd.h>


struct async_struct{
    uint8_t *uid;
    void *dat;
    void (*ptr)(int, void*);
};

__attribute__ ((visibility("default")))
char *pia_get_firmware_version(int fd, enum pia_status *status)
{
    struct response r = i2c_call(fd, 0x7F, NULL, 0, 0);
    if(r.result != 0)
    {
        if(pia_verbosity >= ERR)
            fprintf(stderr, "[ERR] Could not get firmware version\n");
        *status = I2C_RESULT_FAIL;
        return NULL;
    }

    return (char*) r.data;
}


uint8_t put_hmac_key(int fd, uint8_t *key)
{
    if(pia_verbosity >= DEBUG)
    {
        fprintf(stderr, "[INF] Setting HMAC key: ");
        for(uint8_t i=0; i<=64; i++)
            fprintf(stderr, "%02x ", key[i]);
        fprintf(stderr, "\n");
    }
    //assert(strlen(key) == 64);
    struct response r = i2c_call(fd, 0x70, key, 64, 0);
    // if(r.result == 0x0b)
    //     printf("Invalid HMAC Key\n");

    if(r.result != 0)
    {
        if(pia_verbosity >= ERR)
            fprintf(stderr, "[ERR] Can't set HMAC key (error 0x%02x)\n", r.result);
        return 0;
    }
    if(r.status != OKAY)
    {
        if(pia_verbosity >= ERR)
            fprintf(stderr, "[ERR] Can't set HMAC key\n");
        return 0;
    }
    return 1;
}


__attribute__ ((visibility("default")))
int pia_generate_hmac(int fd, char *filename)
{
    uint8_t hmac_key[64];
    if (!RAND_bytes(hmac_key, 64)) {
        if(pia_verbosity >= ERR)
            fprintf(stderr, "[ERR] Couldn't obtain random bytes for HMAC key\n");
        return -1;
    }

    if(put_hmac_key(fd, hmac_key) == 0) {
        if(pia_verbosity >= ERR)
            fprintf(stderr, "[ERR] Couldn't set HMAC\n");
        return -1;
    }

    FILE *f = fopen(filename, "wb");
    fwrite(hmac_key, sizeof(hmac_key), 1, f);
    fclose(f);

    return 0;
}


__attribute__ ((visibility("default")))
int pia_verify(int fd, uint8_t *uid, uint8_t *hmac_key, int timeout, enum pia_status *status)
{
    uint8_t hmac_msg[33];
    struct response r;
    int finger_index;
    uint8_t full_msg[9+32+32+1];
    uint8_t *dat;

    if (!RAND_bytes(hmac_msg, sizeof hmac_msg)) {
        if(pia_verbosity >= ERR)
            fprintf(stderr, "[ERR] Couldn't obtain random bytes for HMAC msg\n");
        *status = GENERIC_FAILURE;
        return -1;
    }

    r = i2c_call(fd, 0x02, hmac_msg, 32, timeout);
    if(r.status != OKAY)
    {
        if(r.status == TIMEOUT)
        {
            sleep(1);
            pia_cancel(fd);
            sleep(1);
            pia_cancel(fd);
        }
        *status = r.status;
        return -1;
    }

    if(r.result != 0)
    {
        *status = r.result;
        switch(r.result)
        {
            case 0x00: *status = OKAY; break;
            case 0x01: *status = NOMATCH; break;
            case 0x02: *status = CANCEL; break;
            case 0x03: *status = NO_TEMPLATE; break;
            case 0x05: *status = PARTIAL_IMAGE; break;
            case 0x06: *status = PRESS_TOO_FAST; break;
            case 0x07: *status = BAD_IMAGE; break;
            case 0x0b: *status = INVALID_HMAC_MSG; break;
            default:
                if(pia_verbosity >= ERR)
                    fprintf(stderr, "[ERR] Unknown error 0x%02x\n", r.result);
        }
        if(pia_verbosity >= ERR)
            fprintf(stderr, "[ERR] Verify failed, error 0x%02x\n", r.result);
        return -1;
    }

    if(pia_verbosity >= DEBUG)
    {
        fprintf(stderr, "[INF] Verification reply: ");
        for(uint8_t i=0; i<=r.len; i++)
            fprintf(stderr, "%02x ", r.data[i]);
        fprintf(stderr, "\n");
    }
    dat = r.data;

    full_msg[0] = 0x55;
    full_msg[1] = 0xAA;
    full_msg[2] = 0x00;
    full_msg[3] = 0x82;
    full_msg[4] = 0x00;
    full_msg[5] = 0x00;
    full_msg[6] = 0x00;
    full_msg[7] = 0x21;

    finger_index = full_msg[8] = *dat++;

    memcpy(full_msg +  9, uid, 32);
    memcpy(full_msg + 41, hmac_msg, 32);

    uint8_t *result = HMAC(
        EVP_sha256(),
        hmac_key, 64,
        full_msg, 73,
        NULL, NULL
    );
    for(uint8_t i=0; i<32; i++)
    {
        if(*result++ != *dat++)
        {
            *status = HMAC_FAILED;
            if(pia_verbosity >= ERR)
                fprintf(stderr, "[ERR] HMAC verification failed\n");
            return -1;
        }
    }
    *status = OKAY;
    return finger_index;
}


enum pia_status enroll_step(int fd, uint8_t *uid, int timeout)
{
    struct response r = i2c_call(fd, 0x01, uid, 32, timeout);
    if(r.status != OKAY)
    {
        if(r.status == TIMEOUT)
        {
            sleep(1);
            pia_cancel(fd);
            sleep(1);
            pia_cancel(fd);
        }
        return r.status;
    }

    switch(r.result)
    {
        case 0x00: return OKAY; break;
        case 0x02: return CANCEL; break;
        case 0x08: return STORAGE_FULL; break;
        case 0x0B: return INVALID_UID; break;
        case 0x19: return REDUNDANT; break;
        case 0x20: return BAD_IMAGE; break;
        case 0x21: return GOOD_IMAGE; break;
        case 0x25: return WAIT_FOR_FINGER; break;
        case 0x27: return PRESS_TOO_FAST; break;
        case 0x28: return PARTIAL_IMAGE; break;
    }
    if(pia_verbosity >= ERR)
        fprintf(stderr, "[ERR] Unknown error 0x%02x\n", r.result);
    return UNKNOWN_ERROR;
}


char *enroll_status2string(enum pia_status s)
{
    if(s == OKAY)
        return "Success";
    if(s == CANCEL)
        return "Operation cancelled";
    if(s == STORAGE_FULL)
        return "Data storage is full";
    if(s == INVALID_UID)
        return "UID is invalid";
    if(s == REDUNDANT)
        return "Redundant fingerprint";
    if(s == BAD_IMAGE)
        return "Bad image";
    if(s == GOOD_IMAGE)
        return "Good image capture, next image";
    if(s == WAIT_FOR_FINGER)
        return "Wait for finger press";
    if(s == PRESS_TOO_FAST)
        return "Pressed finger too fast";
    if(s == PARTIAL_IMAGE)
        return "Only partial image captured";
    if(s == GENERIC_FAILURE)
        return "Unknown failure";

    return "Unknown entry";
}


__attribute__ ((visibility("default")))
uint8_t pia_enroll(int fd, uint8_t *uid, int timeout)
{
    enum pia_status s;
    while(1)
    {
        s = enroll_step(fd, uid, timeout);
        printf("Status: %s\n", enroll_status2string(s));
        if(s == OKAY)
            return 1;
        else if( (s == CANCEL) ||
                 (s == STORAGE_FULL) ||
                 (s == INVALID_UID) ||
                 (s == TIMEOUT) ||
                 (s == GENERIC_FAILURE) )
            return 0;
    }
}


__attribute__ ((visibility("default")))
uint8_t pia_get_list(int fd, uint8_t **ans)
{
    struct response r = i2c_call(fd, 0x05, NULL, 0, 0);
    *ans = r.data;
    return r.len;
}


__attribute__ ((visibility("default")))
enum pia_status pia_delete_finger(int fd, uint8_t index)
{
    struct response r = i2c_call(fd, 0x03, &index, 1, 0);
    if(r.status != OKAY)
        return r.status;

    switch(r.result)
    {
        case 0x00: return OKAY; break;
        case 0x0b: return NOT_FOUND; break;
    }
    if(pia_verbosity >= ERR)
        fprintf(stderr, "[ERR] Unknown error 0x%02x\n", r.result);
    return UNKNOWN_ERROR;
}


__attribute__ ((visibility("default")))
void pia_cancel(int fd)
{
    i2c_send(fd, 0x04, NULL, 0);
}


__attribute__ ((visibility("default")))
int pia_verify_full(uint8_t *uid)
{
    int fd;
    char hmac_file[100] = "/etc/pia/hmac";
    enum pia_status status;
    int ind;
    FILE *f;
    uint8_t hmac_key[64];

    if(pia_verbosity >= DEBUG)
        fprintf(stderr, "[ERR] Authenticating %s\n", uid);

    f = fopen(hmac_file, "rb");
    fread(&hmac_key,sizeof(hmac_key),1,f);
    fclose(f);

    fd = pia_open(2, 0x28);
    if(fd < 0) {
        ind = -1;
    }
    else {
        ind = pia_verify(fd, uid, hmac_key, 5, &status);
        pia_close(fd);
    }
    return ind;
}


void* run_auth(void*args)
{
    uint8_t *uid = ((struct async_struct*)args)->uid;
    void *dat = ((struct async_struct*)args)->dat;
    void (*ptr)(int,void*) = ((struct async_struct*)args)->ptr;
    int ind = pia_verify_full(uid);

    (*ptr)(ind, dat);

    free(args);
    pthread_exit(NULL);

    return NULL;
}


__attribute__ ((visibility("default")))
void pia_verify_async(uint8_t *uid, void (*ptr)(int,void*), void *dat)
{
    struct async_struct *args = malloc(sizeof(struct async_struct));
    args->uid = uid;
    args->ptr = ptr;
    args->dat = dat;
    pthread_t thread_id;
    pthread_create(&thread_id, NULL, run_auth, (void *) args);
}
