#ifndef PIA_H
#define PIA_H
#include <stdint.h>

#define LOCKFILE "/tmp/pia.lock"

enum verbosity{
    QUIET = 0,
    ERR = 1,
    WARN = 2,
    DEBUG = 3
};

enum pia_status{
    OKAY,
    I2C_RESULT_FAIL,
    I2C_INCORRECT_HEADER,
    I2C_INCORRECT_COMMAND,
    I2C_COMMAND_FAILED,
    I2C_SECURE_LEVEL_ERROR,
    I2C_RESERVED_BYTES_ERROR,
    I2C_CHECKSUM_ERROR,
    I2C_CHECKSUM_FAIL,

    NOMATCH,
    CANCEL,
    NO_TEMPLATE,
    PARTIAL_IMAGE,
    PRESS_TOO_FAST,
    BAD_IMAGE,
    INVALID_HMAC_MSG,
    HMAC_FAILED,
    GENERIC_FAILURE,

    STORAGE_FULL,
    INVALID_UID,
    REDUNDANT,
    GOOD_IMAGE,
    WAIT_FOR_FINGER,

    NOT_FOUND,
    UNKNOWN_ERROR,
    TIMEOUT
};


struct response {
    enum pia_status status;
    uint8_t result;
    uint16_t len;
    uint8_t *data;
};

struct response i2c_call(int fd, uint8_t cmd, uint8_t *data, uint8_t len, int timeout);
int i2c_send(int fd, uint8_t cmd, uint8_t *data, uint8_t len);

char *pia_get_firmware_version(int fd, enum pia_status *status);
int pia_generate_hmac(int fd, char *filename);
int pia_verify(int fd, uint8_t *uid, uint8_t *hmac_key, int timeout, enum pia_status *status);
uint8_t pia_enroll(int fd, uint8_t *uid, int timeout);
uint8_t pia_get_list(int fd, uint8_t **ans);
enum pia_status pia_delete_finger(int fd, uint8_t index);
void pia_cancel(int fd);
void pia_verify_async(uint8_t *uid, void (*ptr)(int,void*), void *dat);

int pia_open(int adapter, int addr);
void pia_close(int fd);
int pia_verify_full(uint8_t *uid);
extern enum verbosity pia_verbosity;


#endif
