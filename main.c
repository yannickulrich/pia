#include "pia.h"
#include <stdio.h>
#include <getopt.h>
#include <stdlib.h>
#include <string.h>

void pad_string(char *inp, unsigned char *ans)
{
    uint8_t l;
    l = 32 - strlen(inp);
    memset(ans, '0', 32);
    memcpy(ans + l, inp, strlen(inp));
    ans[32] = 0;
}

int main(int argc, char** argv)
{
    int fd = pia_open(2, 0x28);
    unsigned char uid[33];
    enum pia_status s;
    uint8_t *list;
    uint8_t len, ret, c;
    int ind, timeout;
    FILE *f;
    uint8_t hmac_key[64];

    char hmac_file[100] = "/etc/pia/hmac";
    timeout = 0;

    if(fd < 0) return -1;

    ret = 0;
    while((c = getopt(argc, argv, "t:k:v:e:c:ld:VCg")) != -1) {
        switch(c)
        {
            case 't':
                timeout = atoi(optarg);
                break;

            case 'k':
                strcpy(hmac_file, optarg);
                break;

            case 'v':
                pia_verbosity = atoi(optarg);
                break;

            case 'e':
                pad_string(optarg, uid);
                pia_enroll(fd, uid, timeout);
                break;

            case 'c':
                f = fopen(hmac_file, "rb");
                fread(&hmac_key,sizeof(hmac_key),1,f);
                fclose(f);

                pad_string(optarg, uid);
                ind = pia_verify(fd, uid, hmac_key, timeout, &s);
                if(ind == -1)
                    ret = 1;
                break;

            case 'l':
                len = pia_get_list(fd, &list);
                printf("Found %d fingers:\n", len);
                while(len--)
                    printf("Finger %d\n", *list++);
                break;

            case 'd':
                if(pia_delete_finger(fd, atoi(optarg)) == NOT_FOUND)
                {
                    printf("Couldn't find finger %d\n", atoi(optarg));
                    ret = 1;
                }
                break;

            case 'V':
                printf("Firmware version: %s\n",
                       pia_get_firmware_version(fd, &s));
                break;

            case 'C':
                pia_cancel(fd);
                break;

            case 'g':
                pia_generate_hmac(fd, hmac_file);
                break;

            default:
                pia_close(fd);
                return ret;
        }
    }

    pia_close(fd);
    return ret;
}
